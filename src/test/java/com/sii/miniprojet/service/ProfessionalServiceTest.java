package com.sii.miniprojet.service;
import com.sii.miniprojet.Dto.ProfessionalDTO;
import com.sii.miniprojet.model.Professional;
import com.sii.miniprojet.repository.ProfessionalRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProfessionalServiceTest {

    @InjectMocks
    private ProfessionalService professionalService;

    @Mock
    private ProfessionalRepository professionalRepository;


    @Test
    void testGetAllProfessionals() {
        List<Professional> professionalList = new ArrayList<>();
        professionalList.add(new Professional("123", "test", "test", "test", "test", "test", "test"));

        Page<Professional> expectedPage = new PageImpl<>(professionalList);

        when(professionalRepository.findAll(any(Pageable.class))).thenReturn(expectedPage);

        Page<Professional> result = professionalService.getAllProfessioanls(Pageable.unpaged());

        assertEquals(expectedPage.getContent(), result.getContent());
        assertEquals(expectedPage.getTotalElements(), result.getTotalElements());
        assertEquals(expectedPage.getTotalPages(), result.getTotalPages());
    }

    @Test
    void testGetProfessionalById(){
        Professional expectedProfessional = new Professional("123", "test", "test", "test", "test", "test", "test");

        when(professionalRepository.findById("123")).thenReturn(Optional.of(expectedProfessional));
        Optional<Professional> result = professionalService.getProfessionalById("123");
        assertTrue(result.isPresent());
        assertEquals(expectedProfessional, result.get());
    }

    @Test
    void testAddProfessional() throws IOException {
        Professional expectedProfessional = new Professional("123", "test", "test", "test", "test", "test", "test");
        ProfessionalDTO professionalDTO = new ProfessionalDTO("test","test","test","test","test","test");
        when(professionalRepository.save(any(Professional.class))).thenReturn(expectedProfessional);

        Professional resultProfessional = professionalService.addProfessional(professionalDTO);

        verify(professionalRepository, times(1)).save(any(Professional.class));

        assertEquals(professionalDTO.getDesignation(), resultProfessional.getDesignation());
        assertEquals(professionalDTO.getAdresse(), resultProfessional.getAdresse());
        assertEquals(professionalDTO.getNumeroTelephone(), resultProfessional.getNumeroTelephone());
        assertEquals(professionalDTO.getDescription(), resultProfessional.getDescription());
        assertEquals(professionalDTO.getPhotoUrl(), resultProfessional.getPhotoUrl());
        assertEquals(professionalDTO.getActivite(), resultProfessional.getActivite());
    }

    @Test
    void testUpdateProfessional(){
        String professionalId = "123";
        ProfessionalDTO professionalDTO = new ProfessionalDTO("updated test","updated test","updated test","updated test","updated test","updated test");

        Professional existingProfessional = new Professional(professionalId, "old test", "old test", "old test", "old test", "old test", "old test");

        when(professionalRepository.findById(professionalId)).thenReturn(Optional.of(existingProfessional));

        professionalService.updateProfessional(professionalDTO, professionalId);

        verify(professionalRepository, times(1)).save(existingProfessional);

        assertEquals(professionalDTO.getDesignation(), existingProfessional.getDesignation());
        assertEquals(professionalDTO.getActivite(), existingProfessional.getActivite());
        assertEquals(professionalDTO.getAdresse(), existingProfessional.getAdresse());
        assertEquals(professionalDTO.getDescription(), existingProfessional.getDescription());
        assertEquals(professionalDTO.getNumeroTelephone(), existingProfessional.getNumeroTelephone());
        assertEquals(professionalDTO.getPhotoUrl(), existingProfessional.getPhotoUrl());
    }

    @Test
    void testDeleteProfessional(){
        String professionalId = "123";
        professionalService.deleteProfessional(professionalId);
        verify(professionalRepository, times(1)).deleteById(professionalId);
    }

    @Test
    void testGetProfessionalsByDescription() {
        List<Professional> professionalList = new ArrayList<>();
        String description = "test";
        Pageable pageable = PageRequest.of(0, 10);
        professionalList.add(new Professional("123", "test", "test", "test", "test", "test", "test"));
        Page<Professional> expectedPage = new PageImpl<>(professionalList);
        when(professionalRepository.findByDescriptionContaining(description,pageable)).thenReturn(expectedPage);
        Page<Professional> resultPage = professionalService.getProfessionalsByDescription(description,pageable);
        assertEquals(expectedPage, resultPage);
        verify(professionalRepository, times(1)).findByDescriptionContaining(description, pageable);
    }
}