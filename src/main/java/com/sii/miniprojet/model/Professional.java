package com.sii.miniprojet.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "professional")
@AllArgsConstructor
public class Professional {


    @Id
    private String id;

    private String designation;
    private String activite;
    private String numeroTelephone;
    private String photoUrl;
    private String adresse;
    private String description;

}
