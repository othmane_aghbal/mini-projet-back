package com.sii.miniprojet.service;

import com.sii.miniprojet.model.Admin;
import com.sii.miniprojet.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class AdminService {
    AdminRepository adminRepository;

    @Autowired
    public AdminService(AdminRepository adminRepository){
        this.adminRepository = adminRepository;
    }

    public Optional<Admin> findByEmail(String email) {
        return adminRepository.findByEmail(email);
    }
}
