package com.sii.miniprojet.service;

import com.sii.miniprojet.Dto.ProfessionalDTO;
import com.sii.miniprojet.model.Professional;
import com.sii.miniprojet.repository.ProfessionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProfessionalService {

    ProfessionalRepository professionalRepository;

    @Autowired
    public ProfessionalService(ProfessionalRepository professionalRepository){
        this.professionalRepository = professionalRepository;
    }

    //@Value("${sii.env.FolderImages}")
    //public String UPLOAD_FOLDER_FICHES;


    public Page<Professional> getAllProfessioanls(Pageable pageable){
        return professionalRepository.findAll(pageable);
    }

    public Optional<Professional> getProfessionalById(String id){
        return professionalRepository.findById(id);
    }


    public Professional addProfessional(ProfessionalDTO professionalDTO
    ) throws IOException {

        Professional professional = new Professional(
                UUID.randomUUID().toString(),
                professionalDTO.getDesignation(),
                professionalDTO.getActivite(),
                professionalDTO.getNumeroTelephone(),
                professionalDTO.getPhotoUrl(),
                professionalDTO.getAdresse(),
                professionalDTO.getDescription());

        return professionalRepository.save(professional);
    }

    public void updateProfessional(ProfessionalDTO professionalDTO,String id){
        Professional professional = professionalRepository.findById(id).get();

        professional.setDesignation(professionalDTO.getDesignation());
        professional.setActivite(professionalDTO.getActivite());
        professional.setAdresse(professionalDTO.getAdresse());
        professional.setDescription(professionalDTO.getDescription());
        professional.setPhotoUrl(professionalDTO.getPhotoUrl());
        professional.setNumeroTelephone(professionalDTO.getNumeroTelephone());

        professionalRepository.save(professional);
    }


    public void deleteProfessional(String id){
        professionalRepository.deleteById(id);
    }

    public Page<Professional> getProfessionalsByDescription(String description, Pageable pageable){
        return professionalRepository.findByDescriptionContaining(description, pageable);
    }

}
