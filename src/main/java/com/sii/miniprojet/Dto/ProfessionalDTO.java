package com.sii.miniprojet.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
public class ProfessionalDTO {
    private String designation;
    private String activite;
    private String numeroTelephone;
    private String adresse;
    private String description;
    private String photoUrl;
}
