package com.sii.miniprojet.Security.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponseDTO {
    boolean status;
    String token;
    String error;

    public boolean isAuthenticated(){
        return this.status;
    }
}
