package com.sii.miniprojet.Security.Dto;

import lombok.Data;

@Data
public class AdminRegisterDTO {
    String email;
    String password;
}
