package com.sii.miniprojet.Security.Controller;

import com.sii.miniprojet.Security.Dto.AdminRegisterDTO;
import com.sii.miniprojet.Security.Dto.LoginRequestDTO;
import com.sii.miniprojet.Security.Dto.LoginResponseDTO;
import com.sii.miniprojet.Security.Service.AuthentificationService;
import com.sii.miniprojet.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
//@CrossOrigin(origins = "http://localhost:8083")
public class AuthentificationController {

    AuthentificationService authentificationService;

    @Autowired
    public AuthentificationController(AuthentificationService authentificationService){
        this.authentificationService = authentificationService;
    }
    // Changement dans return
    // J'ai ajouté une fct isAuthenticated
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(@RequestBody LoginRequestDTO requestDTO){
        LoginResponseDTO res = authentificationService.login(requestDTO);
        if(res.isAuthenticated()){
            return new ResponseEntity<>(res, HttpStatus.OK);
        }
        return new ResponseEntity<>(res, HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/register")
    public ResponseEntity<Admin> register(@RequestBody AdminRegisterDTO registerDTO){
        Admin admin = authentificationService.registration(registerDTO);
        return new ResponseEntity<>(admin,HttpStatus.OK);
    }

}
