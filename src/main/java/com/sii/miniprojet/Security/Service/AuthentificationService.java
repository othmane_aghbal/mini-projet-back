package com.sii.miniprojet.Security.Service;

import com.sii.miniprojet.Security.Dto.AdminRegisterDTO;
import com.sii.miniprojet.Security.Dto.LoginRequestDTO;
import com.sii.miniprojet.Security.Dto.LoginResponseDTO;
import com.sii.miniprojet.model.Admin;
import com.sii.miniprojet.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class AuthentificationService {

    @Autowired
    AdminRepository adminRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    JwtTokenService tokenService;


    public LoginResponseDTO login(LoginRequestDTO loginRequestDTO) {
        Optional<Admin> adminOptional = adminRepository.findByEmail(loginRequestDTO.getEmail());
        if (adminOptional.isPresent()) {
            Admin admin = adminOptional.get();
            try {
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(admin.getEmail(), loginRequestDTO.getPassword())
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
                String token = tokenService.generateToken(authentication);
                return new LoginResponseDTO(true, token, "none");
            } catch (Exception e) {
                return new LoginResponseDTO(false, "", "credentials are not correct");
            }
        } else {
            return new LoginResponseDTO(false, "", "you don't have an account! sign up first.");
        }
    }


    public Admin registration(AdminRegisterDTO registerDTO){
        if(adminRepository.findByEmail(registerDTO.getEmail()).isPresent()){
            //need to implement the code for the case where duplicated usernames
            return null;
        }
        Admin newAdmin = new Admin();
        newAdmin.setEmail(registerDTO.getEmail());
        newAdmin.setPassword(passwordEncoder.encode(registerDTO.getPassword()));

        return adminRepository.save(newAdmin);
    }

}
