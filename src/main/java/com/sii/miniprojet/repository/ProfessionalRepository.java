package com.sii.miniprojet.repository;

import com.sii.miniprojet.model.Professional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfessionalRepository extends MongoRepository<Professional,String> {
    Page<Professional> findByDescriptionContaining(String description, Pageable pageable);
}
