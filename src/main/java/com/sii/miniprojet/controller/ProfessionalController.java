package com.sii.miniprojet.controller;

import com.sii.miniprojet.Dto.ProfessionalDTO;
import com.sii.miniprojet.model.Professional;
import com.sii.miniprojet.service.ProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/pros")
public class ProfessionalController {


    ProfessionalService professionalService;

    @Autowired
    public ProfessionalController(ProfessionalService professionalService){
        this.professionalService = professionalService;
    }

    @PostMapping("/add")
    public void saveProfessional(@RequestBody ProfessionalDTO professionalDTO) throws IOException {
        professionalService.addProfessional(professionalDTO);
    }

    @GetMapping("")
    public Page<Professional> getProfessionals(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size){
        return professionalService.getAllProfessioanls(PageRequest.of(page,size));
    }

    @GetMapping("/search/{description}")
    public Page<Professional> getProfessionalsByDescription(@PathVariable String description,@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size){
        return professionalService.getProfessionalsByDescription(description,PageRequest.of(page,size));
    }

    @GetMapping("/{id}")
    public Professional getProfessional(@PathVariable String id){
        return professionalService.getProfessionalById(id).get();
    }

    @PutMapping("/update/{id}")
    public void UpdateProfessional(@RequestBody ProfessionalDTO professionalDTO,@PathVariable String id) throws IOException{
        professionalService.updateProfessional(professionalDTO,id);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteProfessional(@PathVariable String id){
        professionalService.deleteProfessional(id);
    }
}
